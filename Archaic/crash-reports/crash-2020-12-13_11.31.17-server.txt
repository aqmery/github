---- Minecraft Crash Report ----
// Uh... Did I do that?

Time: 12/13/20 11:31 AM
Description: Exception in server tick loop

java.lang.NullPointerException: Exception in server tick loop
	at java.util.Objects.requireNonNull(Objects.java:203) ~[?:1.8.0_51] {}
	at net.minecraftforge.common.util.NonNullLazy.lambda$of$0(NonNullLazy.java:39) ~[?:?] {re:classloading}
	at net.minecraftforge.common.util.NonNullLazy$$Lambda$11740/1700607574.get(Unknown Source) ~[?:?] {}
	at blusunrize.immersiveengineering.api.excavator.MineralVein.writeToNBT(MineralVein.java:89) ~[?:?] {re:classloading}
	at blusunrize.immersiveengineering.common.IESaveData.func_189551_b(IESaveData.java:112) ~[?:?] {re:classloading}
	at net.minecraft.world.storage.WorldSavedData.func_215158_a(WorldSavedData.java:43) ~[?:?] {re:mixin,re:classloading}
	at net.minecraft.world.storage.DimensionSavedDataManager.func_212775_b(DimensionSavedDataManager.java:128) ~[?:?] {re:classloading}
	at net.minecraft.world.server.ServerWorld.func_73042_a(ServerWorld.java:707) ~[?:?] {re:mixin,pl:accesstransformer:B,re:classloading,pl:accesstransformer:B,xf:fml:immersiveengineering:IE block update callback,pl:mixin:APP:abnormals_core.mixins.json:ServerWorldMixin,pl:mixin:APP:quark.mixins.json:ServerWorldMixin,pl:mixin:A}
	at net.minecraft.world.server.ServerWorld.func_217445_a(ServerWorld.java:692) ~[?:?] {re:mixin,pl:accesstransformer:B,re:classloading,pl:accesstransformer:B,xf:fml:immersiveengineering:IE block update callback,pl:mixin:APP:abnormals_core.mixins.json:ServerWorldMixin,pl:mixin:APP:quark.mixins.json:ServerWorldMixin,pl:mixin:A}
	at net.minecraft.server.MinecraftServer.func_213211_a(MinecraftServer.java:531) ~[?:?] {re:mixin,pl:accesstransformer:B,re:classloading,pl:accesstransformer:B,pl:mixin:APP:byg.mixins.json:server.MixinMinecraftServer,pl:mixin:A}
	at net.minecraft.server.MinecraftServer.func_71217_p(MinecraftServer.java:806) ~[?:?] {re:mixin,pl:accesstransformer:B,re:classloading,pl:accesstransformer:B,pl:mixin:APP:byg.mixins.json:server.MixinMinecraftServer,pl:mixin:A}
	at net.minecraft.server.integrated.IntegratedServer.func_71217_p(IntegratedServer.java:78) ~[?:?] {re:classloading,pl:runtimedistcleaner:A}
	at net.minecraft.server.MinecraftServer.func_240802_v_(MinecraftServer.java:641) [?:?] {re:mixin,pl:accesstransformer:B,re:classloading,pl:accesstransformer:B,pl:mixin:APP:byg.mixins.json:server.MixinMinecraftServer,pl:mixin:A}
	at net.minecraft.server.MinecraftServer.lambda$startServer$0(MinecraftServer.java:232) [?:?] {re:mixin,pl:accesstransformer:B,re:classloading,pl:accesstransformer:B,pl:mixin:APP:byg.mixins.json:server.MixinMinecraftServer,pl:mixin:A}
	at net.minecraft.server.MinecraftServer$$Lambda$14860/1606765774.run(Unknown Source) [?:?] {}
	at java.lang.Thread.run(Thread.java:745) [?:1.8.0_51] {}


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.16.4
	Minecraft Version ID: 1.16.4
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_51, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 891668728 bytes (850 MB) / 5164236800 bytes (4925 MB) up to 5523898368 bytes (5268 MB)
	CPUs: 12
	JVM Flags: 5 total; -XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -Xss1M -Xmx5926m -Xms256m -XX:PermSize=256m
	ModLauncher: 8.0.6+85+master.325de55
	ModLauncher launch target: fmlclient
	ModLauncher naming: srg
	ModLauncher services: 
		/mixin-0.8.2.jar mixin PLUGINSERVICE 
		/eventbus-3.0.5-service.jar eventbus PLUGINSERVICE 
		/forge-1.16.4-35.1.0.jar object_holder_definalize PLUGINSERVICE 
		/forge-1.16.4-35.1.0.jar runtime_enum_extender PLUGINSERVICE 
		/accesstransformers-2.2.0-shadowed.jar accesstransformer PLUGINSERVICE 
		/forge-1.16.4-35.1.0.jar capability_inject_definalize PLUGINSERVICE 
		/forge-1.16.4-35.1.0.jar runtimedistcleaner PLUGINSERVICE 
		/mixin-0.8.2.jar mixin TRANSFORMATIONSERVICE 
		/forge-1.16.4-35.1.0.jar fml TRANSFORMATIONSERVICE 
	FML: 35.1
	Forge: net.minecraftforge:35.1.0
	FML Language Providers: 
		javafml@35.1
		minecraft@1
	Mod List: 
		ftbessentials-1603.1.0.11.jar                     |FTB Essentials                |ftbessentials                 |1603.1.0.11         |DONE      |NOSIGNATURE
		NetherPortalFix_1.16.3-7.2.1.jar                  |NetherPortalFix               |netherportalfix               |7.2.1               |DONE      |NOSIGNATURE
		refinedpipes-0.5.jar                              |Refined Pipes                 |refinedpipes                  |0.5                 |DONE      |NOSIGNATURE
		AdditionalBanners-1.16.4-5.0.2.jar                |AdditionalBanners             |additionalbanners             |5.0.2               |DONE      |ea:45:b3:82:b6:9d:50:16:95:e7:2e:34:e1:92:d5:b4:9b:69:90:d3:4f:2e:71:99:b0:be:40:80:27:1f:3e:b0
		mcw-windows-1.0.2-mc1.16.4.jar                    |Macaw's Windows               |mcwwindows                    |1.0.2               |DONE      |NOSIGNATURE
		torohealth-1.16.4-forge-3.jar                     |ToroHealth                    |torohealth                    |NONE                |DONE      |NOSIGNATURE
		rsgauges-1.16.4-1.2.6-b2.jar                      |Gauges and Switches           |rsgauges                      |1.2.6-b2            |DONE      |bf:30:76:97:e4:58:41:61:2a:f4:30:d3:8f:4c:e3:71:1d:14:c4:a1:4e:85:36:e3:1d:aa:2f:cb:22:b0:04:9b
		BetterCaves-1.16.3-1.0.6.jar                      |YUNG's Better Caves           |bettercaves                   |1.16.3-1.0.6        |DONE      |NOSIGNATURE
		ForgeEndertech-1.16.4-7.0.8.0-build.0028.jar      |Forge Endertech               |forgeendertech                |7.0.8.0             |DONE      |NOSIGNATURE
		BetterTitleScreen-1.16.4-1.10.2.jar               |Better Title Screen           |bettertitlescreen             |1.16.4-1.10.2       |DONE      |NOSIGNATURE
		CTM-MC1.16.1-1.1.1.5.jar                          |ConnectedTexturesMod          |ctm                           |MC1.16.1-1.1.1.5    |DONE      |NOSIGNATURE
		CookingForBlockheads_1.16.3-9.2.2.jar             |Cooking for Blockheads        |cookingforblockheads          |9.2.2               |DONE      |NOSIGNATURE
		Controlling-7.0.0.11.jar                          |Controlling                   |controlling                   |7.0.0.11            |DONE      |NOSIGNATURE
		ReAuth-1.16-Forge-3.9.3.jar                       |ReAuth                        |reauth                        |3.9.3               |DONE      |3d:06:1e:e5:da:e2:ff:ae:04:00:be:45:5b:ff:fd:70:65:00:67:0b:33:87:a6:5f:af:20:3c:b6:a1:35:ca:7e
		Placebo-1.16.3-4.3.3.jar                          |Placebo                       |placebo                       |4.3.3               |DONE      |NOSIGNATURE
		citadel-1.5.3.jar                                 |Citadel                       |citadel                       |1.5.3               |DONE      |NOSIGNATURE
		mightyarchitect-mc1.16.3_v0.5.jar                 |The Mighty Architect          |mightyarchitect               |0.5                 |DONE      |NOSIGNATURE
		ftb-gui-library-1604.1.1.26.jar                   |FTB GUI Library               |ftbguilibrary                 |1604.1.1.26         |DONE      |NOSIGNATURE
		jumbofurnace-1.16.4-2.2.0.1.jar                   |Jumbo Furnace                 |jumbofurnace                  |1.16.4-2.2.0.1      |DONE      |NOSIGNATURE
		MutantBeasts-1.16.4-1.1.3.jar                     |Mutant Beasts                 |mutantbeasts                  |1.16.4-1.1.3        |DONE      |d9:be:bd:b6:9a:e4:14:aa:05:67:fb:84:06:77:a0:c5:10:ec:27:15:1b:d6:c0:88:49:9a:ef:26:77:61:0b:5e
		Bookshelf-1.16.4-9.3.18.jar                       |Bookshelf                     |bookshelf                     |9.3.18              |DONE      |ea:45:b3:82:b6:9d:50:16:95:e7:2e:34:e1:92:d5:b4:9b:69:90:d3:4f:2e:71:99:b0:be:40:80:27:1f:3e:b0
		DarkUtilities-1.16.4-7.0.3.jar                    |Dark Utilities                |darkutils                     |7.0.3               |DONE      |ea:45:b3:82:b6:9d:50:16:95:e7:2e:34:e1:92:d5:b4:9b:69:90:d3:4f:2e:71:99:b0:be:40:80:27:1f:3e:b0
		ProgressiveBosses-2.2.0-mc1.16.x.jar              |Progressive Bosses            |progressivebosses             |2.2.0               |DONE      |NOSIGNATURE
		mcw-doors-1.0.1fix-mc1.16.4.jar                   |Macaw's Doors                 |mcwdoors                      |1.0.1               |DONE      |NOSIGNATURE
		JustEnoughResources-1.16.4-0.12.0.103.jar         |Just Enough Resources         |jeresources                   |0.12.0.103          |DONE      |NOSIGNATURE
		Paraglider-1.16.2-1.3.1.0.jar                     |Paraglider                    |paraglider                    |1.3.1.0             |DONE      |NOSIGNATURE
		absentbydesign-1.16.4-1.2.0.jar                   |Absent By Design Mod          |absentbydesign                |1.16.4-1.2.0        |DONE      |1f:47:ac:b1:61:82:96:b8:47:19:16:d2:61:81:11:60:3a:06:4b:61:31:56:7d:44:31:1e:0c:6f:22:5b:4c:ed
		supplementaries-0.9.9.jar                         |Supplementaries               |supplementaries               |0.9.9               |DONE      |NOSIGNATURE
		refinedstorage-1.9.9.jar                          |Refined Storage               |refinedstorage                |1.9.9               |DONE      |NOSIGNATURE
		Upgraded Netherite 1.16.4 - 1.7.0.jar             |Upgraded Netherite            |upgradednetherite             |1.7.0               |DONE      |NOSIGNATURE
		PackMenu-1.16.3-2.3.0.jar                         |Pack Menu                     |packmenu                      |2.3.0               |DONE      |NOSIGNATURE
		mcw-bridges-1.0.4-mc1.16.4.jar                    |Macaw's Bridges               |mcwbridges                    |1.0.4               |DONE      |NOSIGNATURE
		industrial-foregoing-1.16.4-3.2.5-74675d6.jar     |Industrial Foregoing          |industrialforegoing           |3.2.5               |DONE      |NOSIGNATURE
		AmbientSounds_v3.1.2_mc1.16.4.jar                 |Ambient Sounds                |ambientsounds                 |3.0.3               |DONE      |NOSIGNATURE
		valhelsia_structures-16.0.5.jar                   |Valhelsia Structures          |valhelsia_structures          |16.0.5              |DONE      |NOSIGNATURE
		ftb-ranks-1604.1.1.9.jar                          |FTB Ranks                     |ftbranks                      |1604.1.1.9          |DONE      |NOSIGNATURE
		mcw-trapdors-1.0.0-mc1.16.4.jar                   |Macaw's Trapdoors             |mcwtrpdoors                   |1.0.0               |DONE      |NOSIGNATURE
		fairylights-4.0.3-1.16.4.jar                      |Fairy Lights                  |fairylights                   |4.0.3               |DONE      |NOSIGNATURE
		SpawnerFix-1.16.2-1.0.0.1.jar                     |Spawner Fix                   |sf                            |1.0.0.1             |DONE      |NOSIGNATURE
		curios-forge-1.16.4-4.0.3.0.jar                   |Curios API                    |curios                        |1.16.4-4.0.3.0      |DONE      |NOSIGNATURE
		tetra-1.16.4-3.3.1.jar                            |Tetra                         |tetra                         |3.3.1               |DONE      |NOSIGNATURE
		Curious Armor Stands-1.16.3-2.0.2.jar             |Curious Armor Stands          |curious_armor_stands          |1.16.3-2.0.2        |DONE      |NOSIGNATURE
		Patchouli-1.16.4-48.jar                           |Patchouli                     |patchouli                     |1.16.4-48           |DONE      |NOSIGNATURE
		collective-1.16.4-1.53.jar                        |Collective                    |collective                    |1.53                |DONE      |NOSIGNATURE
		betterweather-1.0.4.jar                           |Better Weather                |betterweather                 |1.0.1               |DONE      |NOSIGNATURE
		DrawersTooltip-1.16.2-2.1.0.jar                   |Drawers Tooltip               |drawerstooltip                |2.1.0               |DONE      |NOSIGNATURE
		tombstone-1.16-6.1.0.jar                          |Corail Tombstone              |tombstone                     |1.16-6.1.0          |DONE      |NOSIGNATURE
		buildersaddition-1.16.4-20201203a.jar             |Builders Crafts & Addition    |buildersaddition              |1.16.4-20201203a    |DONE      |NOSIGNATURE
		Runelic-1.16.4-6.0.2.jar                          |Runelic                       |runelic                       |6.0.2               |DONE      |ea:45:b3:82:b6:9d:50:16:95:e7:2e:34:e1:92:d5:b4:9b:69:90:d3:4f:2e:71:99:b0:be:40:80:27:1f:3e:b0
		curiouselytra-forge-1.16.3-4.0.0.1.jar            |Curious Elytra                |curiouselytra                 |1.16.3-4.0.0.1      |DONE      |NOSIGNATURE
		AI-Improvements-1.16.2-0.3.0.jar                  |AI-Improvements               |aiimprovements                |0.3.0               |DONE      |NOSIGNATURE
		weirdinggadget-1.16.(2-4)-2.2.7.jar               |The Weirding Gadget           |weirdinggadget                |2.2.7               |DONE      |NOSIGNATURE
		The_Undergarden-1.16.3-0.3.8.jar                  |The Undergarden               |undergarden                   |0.3.8               |DONE      |NOSIGNATURE
		BetterBurning-1.16.4-5.0.2.jar                    |BetterBurning                 |betterburning                 |5.0.2               |DONE      |ea:45:b3:82:b6:9d:50:16:95:e7:2e:34:e1:92:d5:b4:9b:69:90:d3:4f:2e:71:99:b0:be:40:80:27:1f:3e:b0
		ServerTabInfo-1.16.4-1.3.3.jar                    |Server Tab Info               |servertabinfo                 |1.3.3               |DONE      |NOSIGNATURE
		BetterMineshafts-Forge-1.16.3-1.1.jar             |YUNG's Better Mineshafts      |bettermineshafts              |1.16.3-1.1          |DONE      |NOSIGNATURE
		forge-1.16.4-geckolib-3.0.0.jar                   |GeckoLib                      |geckolib3                     |3.0.0               |DONE      |NOSIGNATURE
		dungeons_gear-1.16.4-3.0.4.jar                    |Dungeons Gear                 |dungeons_gear                 |3.0.4               |DONE      |NOSIGNATURE
		ClientTweaks_1.16.3-5.2.0.jar                     |Client Tweaks                 |clienttweaks                  |5.2.0               |DONE      |NOSIGNATURE
		doubledoors_1.16.4-2.1.jar                        |Double Doors                  |doubledoors                   |2.1                 |DONE      |NOSIGNATURE
		BedBenefits-1.16.4-4.0.2.jar                      |BedBenefits                   |bedbenefits                   |4.0.2               |DONE      |ea:45:b3:82:b6:9d:50:16:95:e7:2e:34:e1:92:d5:b4:9b:69:90:d3:4f:2e:71:99:b0:be:40:80:27:1f:3e:b0
		spiders-2.0-1.16.4-1.0.4.jar                      |Spiders 2.0                   |spiderstpo                    |1.0.4               |DONE      |NOSIGNATURE
		jei-1.16.4-7.6.0.62.jar                           |Just Enough Items             |jei                           |7.6.0.62            |DONE      |NOSIGNATURE
		AttributeFix-1.16.4-9.0.2.jar                     |AttributeFix                  |attributefix                  |9.0.2               |DONE      |ea:45:b3:82:b6:9d:50:16:95:e7:2e:34:e1:92:d5:b4:9b:69:90:d3:4f:2e:71:99:b0:be:40:80:27:1f:3e:b0
		Masonry_Blocks_1.16.4_1.0.jar                     |Masonry Blocks                |masonry_blocks                |1.0                 |DONE      |NOSIGNATURE
		abnormals_core-1.16.4-3.0.2.jar                   |Abnormals Core                |abnormals_core                |3.0.2               |DONE      |NOSIGNATURE
		caelus-forge-1.16.4-2.1.0.0.jar                   |Caelus API                    |caelus                        |1.16.4-2.1.0.0      |DONE      |NOSIGNATURE
		Waystones_1.16.3-7.3.1.jar                        |Waystones                     |waystones                     |7.3.1               |DONE      |NOSIGNATURE
		Clumps-6.0.0.13.jar                               |Clumps                        |clumps                        |6.0.0.13            |DONE      |NOSIGNATURE
		mgui-1.16.4-3.1.3.jar                             |mgui                          |mgui                          |3.1.3               |DONE      |NOSIGNATURE
		fakeblocks-1.16.4-1.0.1.jar                       |Fake Blocks                   |fakeblocks                    |1.16.4-1.0.1        |DONE      |NOSIGNATURE
		comforts-forge-1.16.4-4.0.0.2.jar                 |Comforts                      |comforts                      |1.16.4-4.0.0.2      |DONE      |NOSIGNATURE
		TravelersBackpack-1.16.4-5.3.1.jar                |Traveler's Backpack           |travelersbackpack             |5.3.1               |DONE      |NOSIGNATURE
		Artifacts-1.16.4-2.7.1.jar                        |Artifacts                     |artifacts                     |1.16.4-2.7.1        |DONE      |NOSIGNATURE
		netheritehorsearmor-1.6.jar                       |Netherite Horse Armor Mod     |netheritehorsearmor           |1.5                 |DONE      |NOSIGNATURE
		decorative_blocks-1.16.4-1.6.0.jar                |Decorative Blocks             |decorative_blocks             |1.6.0               |DONE      |NOSIGNATURE
		DungeonCrawl-1.16.3-2.2.2.jar                     |Dungeon Crawl                 |dungeoncrawl                  |2.2.2               |DONE      |NOSIGNATURE
		curioofundying-forge-1.16.4-5.1.0.0.jar           |Curio of Undying              |curioofundying                |1.16.4-5.1.0.0      |DONE      |NOSIGNATURE
		FarmingForBlockheads_1.16.3-7.2.1.jar             |Farming for Blockheads        |farmingforblockheads          |7.2.1               |DONE      |NOSIGNATURE
		additional_lights-1.16.4-2.1.3.jar                |Additional Lights             |additional_lights             |2.1.3               |DONE      |NOSIGNATURE
		iceandfire-2.1.4-1.16.4.jar                       |Ice and Fire                  |iceandfire                    |2.1.4-1.16.4        |DONE      |NOSIGNATURE
		JEITweaker-1.16.4-1.0.1.5.jar                     |JEI Tweaker                   |jeitweaker                    |1.0.1.5             |DONE      |NOSIGNATURE
		iChunUtil-1.16.3-10.0.0.jar                       |iChunUtil                     |ichunutil                     |10.0.0              |DONE      |NOSIGNATURE
		CraftTweaker-1.16.4-7.0.0.63.jar                  |CraftTweaker                  |crafttweaker                  |7.0.0.63            |DONE      |NOSIGNATURE
		ars_nouveau-1.16.3-1.8.1.jar                      |Ars Nouveau                   |ars_nouveau                   |1.8.1               |DONE      |NOSIGNATURE
		mysticalworld-1.16.4-0.3.1.2.jar                  |Mystical World                |mysticalworld                 |1.16.4-0.3.1.2      |DONE      |NOSIGNATURE
		ImmersivePetroleum-1.16.3-3.0.0.jar               |Immersive Petroleum           |immersivepetroleum            |3.0.0               |DONE      |NOSIGNATURE
		ftb-chunks-1604.2.1.50.jar                        |FTB Chunks                    |ftbchunks                     |1604.2.1.50         |DONE      |NOSIGNATURE
		forge-1.16.4-35.1.0-universal.jar                 |Forge                         |forge                         |35.1.0              |DONE      |22:af:21:d8:19:82:7f:93:94:fe:2b:ac:b7:e4:41:57:68:39:87:b1:a7:5c:c6:44:f9:25:74:21:14:f5:0d:90
		scuba-gear-1.16.4-1.0.1.jar                       |Scuba Gear                    |scuba_gear                    |1.0.1               |DONE      |NOSIGNATURE
		ironchest-1.16.4-11.2.10.jar                      |Iron Chests                   |ironchest                     |1.16.4-11.2.10      |DONE      |NOSIGNATURE
		forge-1.16.4-35.1.0-client.jar                    |Minecraft                     |minecraft                     |1.16.4              |DONE      |NOSIGNATURE
		cofh_core-1.16.3-1.0.4.jar                        |CoFH Core                     |cofh_core                     |1.0.4               |DONE      |NOSIGNATURE
		ensorcellation-1.16.3-1.0.3.jar                   |Ensorcellation                |ensorcellation                |1.0.3               |DONE      |NOSIGNATURE
		MouseTweaks-2.13-mc1.16.2.jar                     |Mouse Tweaks                  |mousetweaks                   |2.13                |DONE      |NOSIGNATURE
		titanium-1.16.4-3.2.2.jar                         |Titanium                      |titanium                      |3.2.2               |DONE      |NOSIGNATURE
		ImmersiveEngineering-1.16.4-4.1.1-128.jar         |Immersive Engineering         |immersiveengineering          |1.16.4-4.1.1-128    |DONE      |44:39:94:cf:1d:8c:be:3c:7f:a9:ee:f4:1e:63:a5:ac:61:f9:c2:87:d5:5b:d9:d6:8c:b5:3e:96:5d:8e:3f:b7
		EmendatusEnigmatica-1.16.4-1.0.2.jar              |Emendatus Enigmatica          |emendatusenigmatica           |1.16.4-1.0.2        |DONE      |NOSIGNATURE
		CreativeCore_v2.0.10_mc1.16.4.jar                 |CreativeCore                  |creativecore                  |2.0.0               |DONE      |NOSIGNATURE
		AdLods-1.16.4-4.1.2.0-build.0027.jar              |Large Ore Deposits            |adlods                        |4.1.2.0             |DONE      |NOSIGNATURE
		towers_of_the_wild-1.16.4-2.0.1.jar               |Towers Of The Wild            |towers_of_the_wild            |1.16.4-2.0.1        |DONE      |NOSIGNATURE
		jeiintegration_1.16.4-6.1.1.11.jar                |JEI Integration               |jeiintegration                |6.1.1.11            |DONE      |NOSIGNATURE
		FastWorkbench-1.16.3-4.4.1.jar                    |FastWorkbench                 |fastbench                     |4.4.1               |DONE      |NOSIGNATURE
		polymorph-forge-1.16.4-0.18.jar                   |Polymorph                     |polymorph                     |1.16.4-0.18         |DONE      |NOSIGNATURE
		AutoRegLib-1.6-47.jar                             |AutoRegLib                    |autoreglib                    |1.6-47              |DONE      |NOSIGNATURE
		Quark-r2.4-279.jar                                |Quark                         |quark                         |r2.4-279            |DONE      |NOSIGNATURE
		StorageDrawers-1.16.3-8.2.1.jar                   |Storage Drawers               |storagedrawers                |8.2.1               |DONE      |NOSIGNATURE
		WorldPreGenerator-1.16.4-1.0.0.jar                |World Pre Generator           |world_pre_generator           |1.0.0               |DONE      |NOSIGNATURE
		InventoryHud_1.16.x.forge-3.1.4.jar               |Inventory HUD+(Forge edition) |inventoryhud                  |3.1.4               |DONE      |NOSIGNATURE
		structurize-0.13.96-ALPHA-universal.jar           |Structurize                   |structurize                   |0.13.96-ALPHA       |DONE      |NOSIGNATURE
		minecolonies-0.13.437-RELEASE-universal.jar       |Minecolonies                  |minecolonies                  |0.13.437-RELEASE    |DONE      |NOSIGNATURE
		FastFurnace-1.16.3-4.3.0.jar                      |FastFurnace                   |fastfurnace                   |4.3.0               |DONE      |NOSIGNATURE
		AppleSkin-mc1.16.2-forge-1.0.14.jar               |AppleSkin                     |appleskin                     |1.0.14              |DONE      |NOSIGNATURE
		engineersdecor-1.16.4-1.1.4-b2.jar                |Engineer's Decor              |engineersdecor                |1.1.4-b2            |DONE      |bf:30:76:97:e4:58:41:61:2a:f4:30:d3:8f:4c:e3:71:1d:14:c4:a1:4e:85:36:e3:1d:aa:2f:cb:22:b0:04:9b
		MineTogether-1.16.4-4.1.6RC3.jar                  |MineTogether                  |minetogether                  |1.16.4-4.1.6RC3     |DONE      |ef:3c:f1:a8:ac:2b:fc:1d:45:a5:19:41:29:40:fe:b5:2f:62:f0:55:26:c2:d5:c2:8c:b3:dd:d4:ef:c8:93:b0
		BackTools-1.16.3-10.0.0.jar                       |Back Tools                    |backtools                     |10.0.0              |DONE      |NOSIGNATURE
		byg-1.1.5.jar                                     |Oh The Biomes You'll Go       |byg                           |1.1.5               |DONE      |NOSIGNATURE
		Aquaculture-1.16.4-2.1.7.jar                      |Aquaculture 2                 |aquaculture                   |1.16.4-2.1.7        |DONE      |NOSIGNATURE
		refinedstorageaddons-0.7.2.jar                    |Refined Storage Addons        |refinedstorageaddons          |0.7.2               |DONE      |NOSIGNATURE
		CosmeticArmorReworked-1.16.4-v1.jar               |CosmeticArmorReworked         |cosmeticarmorreworked         |1.16.4-v1           |DONE      |5e:ed:25:99:e4:44:14:c0:dd:89:c1:a9:4c:10:b5:0d:e4:b1:52:50:45:82:13:d8:d0:32:89:67:56:57:01:53
		overloadedarmorbar-5.1.0.jar                      |Overloaded Armor Bar          |overloadedarmorbar            |5.1.0               |DONE      |NOSIGNATURE
		chiselsandbits-0.2.8-RELEASE.jar                  |Chisels & bits                |chiselsandbits                |NONE                |DONE      |NOSIGNATURE
		DefaultOptions_1.16.3-12.2.0.jar                  |Default Options               |defaultoptions                |12.2.0              |DONE      |NOSIGNATURE
	Crash Report UUID: 6b726c03-0d6e-4808-90c5-ce64ae2a78dd
	Patchouli open book context: n/a
	Player Count: 1 / 8; [ServerPlayerEntity['_Acheron'/168, l='ServerLevel[New World]', x=-3312.22, y=115.23, z=141.66]]
	Data Packs: vanilla, mod:ftbessentials, mod:netherportalfix (incompatible), mod:refinedpipes, mod:additionalbanners, mod:mcwwindows (incompatible), mod:torohealth, mod:rsgauges, mod:bettercaves (incompatible), mod:forgeendertech, mod:bettertitlescreen, mod:ctm (incompatible), mod:cookingforblockheads (incompatible), mod:controlling, mod:reauth, mod:placebo (incompatible), mod:citadel (incompatible), mod:ftbguilibrary (incompatible), mod:jumbofurnace (incompatible), mod:mutantbeasts (incompatible), mod:bookshelf, mod:darkutils (incompatible), mod:progressivebosses, mod:mcwdoors, mod:jeresources, mod:paraglider, mod:absentbydesign (incompatible), mod:supplementaries, mod:refinedstorage, mod:packmenu (incompatible), mod:mcwbridges (incompatible), mod:industrialforegoing (incompatible), mod:ambientsounds (incompatible), mod:valhelsia_structures (incompatible), mod:ftbranks (incompatible), mod:mcwtrpdoors (incompatible), mod:fairylights, mod:sf, mod:curios, mod:curious_armor_stands (incompatible), mod:patchouli (incompatible), mod:collective, mod:betterweather (incompatible), mod:drawerstooltip, mod:tombstone, mod:buildersaddition (incompatible), mod:runelic, mod:curiouselytra, mod:aiimprovements, mod:weirdinggadget, mod:undergarden, mod:betterburning, mod:servertabinfo, mod:bettermineshafts (incompatible), mod:geckolib3 (incompatible), mod:dungeons_gear, mod:clienttweaks (incompatible), mod:doubledoors, mod:bedbenefits, mod:spiderstpo, mod:jei, mod:attributefix, mod:abnormals_core, mod:caelus, mod:waystones (incompatible), mod:fakeblocks, mod:comforts (incompatible), mod:travelersbackpack (incompatible), mod:artifacts, mod:netheritehorsearmor, mod:decorative_blocks, mod:dungeoncrawl, mod:curioofundying, mod:farmingforblockheads (incompatible), mod:additional_lights, mod:iceandfire (incompatible), mod:jeitweaker, mod:ichunutil, mod:crafttweaker, mod:ars_nouveau, mod:mysticalworld (incompatible), mod:immersivepetroleum (incompatible), mod:ftbchunks (incompatible), mod:forge, mod:scuba_gear (incompatible), mod:ironchest (incompatible), mod:cofh_core (incompatible), mod:ensorcellation (incompatible), mod:mousetweaks, mod:titanium (incompatible), mod:immersiveengineering (incompatible), mod:emendatusenigmatica (incompatible), mod:creativecore, mod:adlods, mod:towers_of_the_wild, mod:jeiintegration, mod:fastbench (incompatible), mod:polymorph, mod:autoreglib (incompatible), mod:quark (incompatible), mod:storagedrawers (incompatible), mod:world_pre_generator, mod:inventoryhud, mod:structurize, mod:minecolonies (incompatible), mod:fastfurnace (incompatible), mod:appleskin, mod:engineersdecor, mod:minetogether (incompatible), mod:backtools, mod:byg, mod:aquaculture (incompatible), mod:refinedstorageaddons, mod:cosmeticarmorreworked (incompatible), mod:overloadedarmorbar (incompatible), mod:chiselsandbits (incompatible), mod:defaultoptions (incompatible), mod:mightyarchitect (incompatible), mod:upgradednetherite (incompatible), mod:tetra, mod:masonry_blocks, mod:clumps, mod:mgui (incompatible)
	Type: Integrated Server (map_client.txt)
	Is Modded: Definitely; Client brand changed to 'forge'